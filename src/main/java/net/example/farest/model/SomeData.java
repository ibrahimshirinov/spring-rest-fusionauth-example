package net.example.farest.model;

public record SomeData(String allowed,
                       boolean authenticated,
                       String authorities) {
}

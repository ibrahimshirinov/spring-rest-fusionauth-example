package net.example.farest.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import net.example.farest.model.SomeData;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class BasicController {

    @Operation(summary = "Get some data for anyone")
    @ApiResponses({
      @ApiResponse(responseCode = "200",
        description = "Success",
        content = {
          @Content(mediaType = "application/json",
            schema = @Schema(implementation = SomeData.class))})
    })
    @GetMapping("/anyone")
    @PreAuthorize("permitAll()")
    public SomeData allowAnyone(Authentication authentication) {
        return new SomeData("Anyone",
                isAuthenticated(authentication),
                getAuthorities(authentication));
    }

    @Operation(summary = "Get some data for basic users")
    @ApiResponses({
      @ApiResponse(responseCode = "200",
        description = "Success",
        content = {
          @Content(mediaType = "application/json",
            schema = @Schema(implementation = SomeData.class))})
    })
    @GetMapping("/basic")
    @PreAuthorize("hasAuthority('basic') or hasAuthority('admin')")
    public SomeData allowBasicUser(Authentication authentication) {
        return new SomeData("Basic User",
                isAuthenticated(authentication),
                getAuthorities(authentication));
    }

    @Operation(summary = "Get some data for editor users")
    @ApiResponses({
      @ApiResponse(responseCode = "200",
        description = "Success",
        content = {
          @Content(mediaType = "application/json",
            schema = @Schema(implementation = SomeData.class))})
    })
    @GetMapping("/editor")
    @PreAuthorize("hasAuthority('editor') or hasAuthority('admin')")
    public SomeData allowEditorUser(Authentication authentication) {
        return new SomeData("Editor User",
                isAuthenticated(authentication),
                getAuthorities(authentication));
    }

    @Operation(summary = "Get some data for admin users")
    @ApiResponses({
      @ApiResponse(responseCode = "200",
        description = "Success",
        content = {
          @Content(mediaType = "application/json",
            schema = @Schema(implementation = SomeData.class))})
    })
    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('admin')")
    public SomeData allowAdminUser(Authentication authentication) {
        return new SomeData("Admin User",
                isAuthenticated(authentication),
                getAuthorities(authentication));
    }

    private boolean isAuthenticated(Authentication authentication) {
        return authentication != null && authentication.isAuthenticated();
    }

    private String getAuthorities(Authentication authentication) {
        return isAuthenticated(authentication)
                ? authentication.getAuthorities().toString()
                : "";
    }
}

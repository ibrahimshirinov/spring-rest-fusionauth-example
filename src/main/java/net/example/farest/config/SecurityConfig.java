package net.example.farest.config;

import net.example.farest.security.OidcJwtAuthConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private final OAuth2ResourceServerProperties oauth2Properties;
  private final String issuer;

  @Autowired
  public SecurityConfig(OAuth2ResourceServerProperties oauth2Properties,
                        @Value("${oidc.issuer}") String issuer) {
    this.oauth2Properties = oauth2Properties;
    this.issuer = issuer;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
      .and()
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
        .csrf().disable()
        .authorizeRequests()
          .antMatchers("/api/v1/anyone").permitAll()
          .antMatchers("/swagger-ui/**").permitAll()
          .antMatchers("/v3/api-docs/**").permitAll()
          .anyRequest()
            .fullyAuthenticated()
      .and()
        .oauth2ResourceServer()
          .jwt()
            .jwtAuthenticationConverter(new OidcJwtAuthConverter());
  }

  @Bean
  public JwtDecoder jwtDecoder() {
    final var jwtDecoder =
      NimbusJwtDecoder.withJwkSetUri(oauth2Properties.getJwt().getJwkSetUri())
              .build();
    final var withIssuer = JwtValidators.createDefaultWithIssuer(issuer);
    jwtDecoder.setJwtValidator(withIssuer);
    return jwtDecoder;
  }
}

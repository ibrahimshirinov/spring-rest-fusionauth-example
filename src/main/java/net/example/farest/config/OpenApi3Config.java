package net.example.farest.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OpenApi3Config {
  private final BuildProperties buildProperties;
  private final String authUrl;
  private final String tokenUrl;

  @Autowired
  public OpenApi3Config(BuildProperties buildProperties,
                        @Value("${oidc.auth-url}") String authUrl,
                        @Value("${oidc.token-url}") String tokenUrl) {
    this.buildProperties = buildProperties;
    this.authUrl = authUrl;
    this.tokenUrl = tokenUrl;
  }

  @Bean
  public OpenAPI openAPI() {
    return new OpenAPI()
      .components(new Components()
        .addSecuritySchemes("oauth2", new SecurityScheme()
          .type(SecurityScheme.Type.OAUTH2)
          .description("OAuth2 Flow")
          .flows(new OAuthFlows()
            .authorizationCode(new OAuthFlow()
              .authorizationUrl(authUrl)
              .tokenUrl(tokenUrl)
              .scopes(new Scopes())
            )
          )
        )
      )
      .security(List.of(new SecurityRequirement()
              .addList("oauth2")))
      .info(new Info()
        .title("Simple Example REST Service")
        .description("""
Simple REST Service used to demonstrate
securing a Spring REST service with
FusionAuth.
                     """)
        .version(buildProperties.getVersion())
    );
  }
}
